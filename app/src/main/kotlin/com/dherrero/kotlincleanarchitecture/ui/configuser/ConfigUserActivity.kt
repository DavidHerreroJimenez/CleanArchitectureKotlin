package com.dherrero.kotlincleanarchitecture.ui.configuser

import android.support.v4.app.Fragment
import android.widget.Toast
import com.dherrero.googlesignin.GoogleSignInApi
import com.dherrero.kotlincleanarchitecture.R
import com.dherrero.kotlincleanarchitecture.model.User
import com.dherrero.kotlincleanarchitecture.ui.base.BaseActivity
import com.dherrero.kotlincleanarchitecture.ui.configuser.fragments.ConfigUserFragment
import com.dherrero.kotlincleanarchitecture.ui.configuser.presenter.ConfigUserActivityPresenter
import com.dherrero.kotlincleanarchitecture.ui.configuser.view.ConfigUserActivityView
import com.dherrero.kotlincleanarchitecture.ui.configuser.view.ConfigUserInterfaceConnectFragmentsWithActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.fragment_configuser.*
import javax.inject.Inject

/**
 * Created by dherrero on 28/02/2018.
 */
class ConfigUserActivity : BaseActivity(), HasSupportFragmentInjector, ConfigUserActivityView, ConfigUserInterfaceConnectFragmentsWithActivity, OnCompleteListener<Void> {

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var configUserActivityPresenter: ConfigUserActivityPresenter

    @Inject
    lateinit var googleSignInApi: GoogleSignInApi


    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_configuser
    }

    override fun setLogicOnCreate() {

        googleSignInApi.getClientForSignInWithGoogle(this)

        supportFragmentManager.beginTransaction().add(R.id.configuser_fragment_container, ConfigUserFragment().newInstance()).commitAllowingStateLoss()
    }


    override fun signOut() {

        googleSignInApi.signOut(this)

    }

    override fun onComplete(p0: Task<Void>) {
        finishActivity()
    }


    fun finishActivity() {
        finish()
    }

//    override fun onDestroy() {
//        super.onDestroy()
//       // configUserActivityPresenter.onDestroy()
//    }

    override fun createNewUserIntoBD() {

        val account = googleSignInApi.getLastSignedInAccount(this)


        if(account != null) {


            val nickname: String = account.displayName?: ""
            val email: String = account.email?: ""
            val firstname = account.givenName?: ""
            val surname = account.familyName?: ""

            var manageURL = account.photoUrl?: ""
            val manageURLString = manageURL.toString()

            val photourl = manageURLString

            val role = 0 //rol en el grupo.

            val superuser: Boolean = false

            val timestamp: Long = System.currentTimeMillis()

            val input: Boolean = true


            configUserActivityPresenter.createNewUser(firstname, surname, nickname, email, role, photourl, superuser, timestamp, input)

        }else{
            Toast.makeText(this,"no se pudo obtener la cuenta", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getUsers(){

        val userRealms: Iterable<User> = configUserActivityPresenter.getUsers()



    }
}