package com.dherrero.kotlincleanarchitecture.ui.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import dagger.android.AndroidInjection

/**
 * Created by dherrero on 26/02/2018.
 */
abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)

        setContentView(getLayoutResource())

        setLogicOnCreate()


    }


    protected abstract fun getLayoutResource(): Int

    protected abstract fun setLogicOnCreate()

}