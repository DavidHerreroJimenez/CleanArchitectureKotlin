package com.dherrero.kotlincleanarchitecture.ui.login.di

import android.support.v4.app.Fragment
import com.dherrero.kotlincleanarchitecture.ui.login.LoginActivity
import com.dherrero.kotlincleanarchitecture.ui.login.fragments.LoginFragment
import com.dherrero.kotlincleanarchitecture.ui.login.fragments.di.LoginFragmentSubComponent
import com.dherrero.kotlincleanarchitecture.ui.login.presenter.LoginActivityPresenter
import com.dherrero.kotlincleanarchitecture.ui.login.presenter.LoginActivityPresenterImpl
import com.dherrero.kotlincleanarchitecture.ui.login.view.LoginActivityView
import com.dherrero.kotlincleanarchitecture.ui.login.view.LoginInterfaceConnectFragmentsWithActivity
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap


/**
 * Created by dherrero on 26/02/2018.
 */
@Module(subcomponents = arrayOf(LoginFragmentSubComponent::class))
abstract class LoginActivityModule {

    @Binds
    @IntoMap
    @FragmentKey(LoginFragment::class)
    internal abstract fun provideLoginFragmentFactory(builder: LoginFragmentSubComponent.Builder): AndroidInjector.Factory<out Fragment>

    @Binds
    internal abstract fun provideLoginActivityPresenter(loginActivityPresenterImpl: LoginActivityPresenterImpl): LoginActivityPresenter

    @Binds
    internal abstract fun provideLoginActivityView(loginActivity: LoginActivity): LoginActivityView

    @Binds
    internal abstract fun provideLoginInterfaceConnectFragmentsWithActivity(loginActivity: LoginActivity): LoginInterfaceConnectFragmentsWithActivity

}