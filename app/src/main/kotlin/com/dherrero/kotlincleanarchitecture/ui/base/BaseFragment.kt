package com.dherrero.kotlincleanarchitecture.ui.base

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import dagger.android.support.AndroidSupportInjection

/**
 * Created by dherrero on 26/02/2018.
 */
abstract class BaseFragment : Fragment() {


    override fun onAttach(context: Context?) {

        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater!!.inflate(getLayoutResource(), container, false)

        setView()


        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setLogicWhenOnViewCreated()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {

        if (menu != null) {
            menu.clear()
        }

        if (inflater != null) {
            inflater.inflate(getMenuResource(), menu)
        }

        setOptionsMenu(menu)



        super.onCreateOptionsMenu(menu, inflater)
    }


    protected abstract fun getLayoutResource(): Int
    protected abstract fun setView()
    protected abstract fun setLogicWhenOnViewCreated()
    protected abstract fun getMenuResource(): Int
    protected abstract fun setOptionsMenu(menu: Menu?)

}