package com.dherrero.kotlincleanarchitecture.ui.configuser.presenter

import com.dherrero.kotlincleanarchitecture.data.realm.db.RealmDB
import com.dherrero.kotlincleanarchitecture.data.realm.repository.UserRealmRepository
import com.dherrero.kotlincleanarchitecture.data.realm.repository.converters.UserToRealmUser
import com.dherrero.kotlincleanarchitecture.model.Time
import com.dherrero.kotlincleanarchitecture.model.User
import javax.inject.Inject

/**
 * Created by dherrero on 28/02/2018.
 */
class ConfigUserActivityPresenterImpl @Inject constructor() : ConfigUserActivityPresenter {



    @Inject
    lateinit var repositoryUsers: UserRealmRepository

    @Inject
    lateinit var realmDB: RealmDB


    override fun createNewUser(firstname: String, surname: String, nickname: String, email: String, role: Int, photourl: String, superuser: Boolean, timestamp : Long, input: Boolean) {

        val time = Time(timestamp, input)

        val user = User(
                firstname,
                surname,
                nickname,
                email,
                role,
                photourl,
                superuser,
                time)


        repositoryUsers.createUser(user)
    }


    override fun getUsers() : Iterable<User> {

        val users = repositoryUsers.findAllUsers()

        return users
    }

    override fun onDestroy() {

            realmDB.closeDB()

    }
}