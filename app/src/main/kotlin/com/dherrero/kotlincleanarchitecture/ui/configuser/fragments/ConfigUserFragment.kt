package com.dherrero.kotlincleanarchitecture.ui.configuser.fragments

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.dherrero.kotlincleanarchitecture.R
import com.dherrero.kotlincleanarchitecture.R.id.sign_out_menu_button
import com.dherrero.kotlincleanarchitecture.ui.base.BaseFragment
import com.dherrero.kotlincleanarchitecture.ui.configuser.view.ConfigUserInterfaceConnectFragmentsWithActivity
import kotlinx.android.synthetic.main.fragment_configuser.*
import javax.inject.Inject


/**
 * Created by dherrero on 28/02/2018.
 */
class ConfigUserFragment @Inject constructor() : BaseFragment(), ConfigUserFragmentView, View.OnClickListener {


    @Inject
    lateinit var configUserInterfaceConnectFragmentsWithActivity: ConfigUserInterfaceConnectFragmentsWithActivity


    fun newInstance(): ConfigUserFragment {

        val args = Bundle()

        val fragment = ConfigUserFragment()
        fragment.setArguments(args)
        return fragment
    }

    override fun getLayoutResource(): Int {

        return R.layout.fragment_configuser
    }

    override fun getMenuResource(): Int {
        return R.menu.menu_configuser
    }

    override fun setOptionsMenu(menu: Menu?) {

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item != null) {
            when (item.itemId) {

                sign_out_menu_button -> {

                    signOut()
                    return true
                }

                else -> return super.onOptionsItemSelected(item)
            }
        } else return super.onOptionsItemSelected(item)
    }

    fun signOut() {

        configUserInterfaceConnectFragmentsWithActivity.signOut()


    }


    override fun setLogicWhenOnViewCreated() {
        checkIfUserExist()

        newUserBtn.setOnClickListener(this)
        getUsersBtn.setOnClickListener(this)
    }

    fun checkIfUserExist() {
        Toast.makeText(context, "Eiii!! ConfigUserFragmentOnViewCreated", Toast.LENGTH_LONG).show()
    }


    override fun onClick(v: View?) {

        if (v != null) {
            when(v.id){
                newUserBtn.id -> createNewUser()

                getUsersBtn.id -> getUsers()
            }

        }


    }

    fun createNewUser(){


        configUserInterfaceConnectFragmentsWithActivity.createNewUserIntoBD()
    }

    fun getUsers(){

        configUserInterfaceConnectFragmentsWithActivity.getUsers()
    }

    override fun setView() {

    }
}