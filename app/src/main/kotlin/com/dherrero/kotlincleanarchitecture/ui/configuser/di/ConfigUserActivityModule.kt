package com.dherrero.kotlincleanarchitecture.ui.configuser.di

import android.support.v4.app.Fragment
import com.dherrero.kotlincleanarchitecture.ui.configuser.ConfigUserActivity
import com.dherrero.kotlincleanarchitecture.ui.configuser.fragments.ConfigUserFragment
import com.dherrero.kotlincleanarchitecture.ui.configuser.fragments.di.ConfigUserFragmentComponent
import com.dherrero.kotlincleanarchitecture.ui.configuser.presenter.ConfigUserActivityPresenter
import com.dherrero.kotlincleanarchitecture.ui.configuser.presenter.ConfigUserActivityPresenterImpl
import com.dherrero.kotlincleanarchitecture.ui.configuser.view.ConfigUserInterfaceConnectFragmentsWithActivity
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap

/**
 * Created by dherrero on 28/02/2018.
 */
@Module(subcomponents = arrayOf(ConfigUserFragmentComponent::class))
abstract class ConfigUserActivityModule {

    @Binds
    @IntoMap
    @FragmentKey(ConfigUserFragment::class)
    internal abstract fun provideConfigUserFragmentFactory(builder: ConfigUserFragmentComponent.Builder): AndroidInjector.Factory<out Fragment>

    @Binds
    internal abstract fun provideConfigUserActivityPresenter(configUserActivityPresenterImpl: ConfigUserActivityPresenterImpl): ConfigUserActivityPresenter


    @Binds
    internal abstract fun provideConfigUserInterfaceConnectFragmentsWithActivity(configUserActivity: ConfigUserActivity): ConfigUserInterfaceConnectFragmentsWithActivity


}