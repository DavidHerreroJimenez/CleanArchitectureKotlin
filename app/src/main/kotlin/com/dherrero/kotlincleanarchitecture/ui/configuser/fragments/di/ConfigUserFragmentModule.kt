package com.dherrero.kotlincleanarchitecture.ui.configuser.fragments.di

import com.dherrero.kotlincleanarchitecture.ui.configuser.fragments.ConfigUserFragment
import com.dherrero.kotlincleanarchitecture.ui.configuser.fragments.ConfigUserFragmentView
import dagger.Module
import dagger.Provides

/**
 * Created by dherrero on 28/02/2018.
 */
@Module
class ConfigUserFragmentModule {

    @Provides
    internal fun provideConfigUserFragmentView(configUserFragment: ConfigUserFragment): ConfigUserFragmentView {
        return configUserFragment
    }


}