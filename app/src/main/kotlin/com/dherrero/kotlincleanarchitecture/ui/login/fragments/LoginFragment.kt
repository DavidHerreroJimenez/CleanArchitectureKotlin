package com.dherrero.kotlincleanarchitecture.ui.login.fragments

import android.os.Bundle
import android.view.Menu
import android.view.View
import com.dherrero.kotlincleanarchitecture.R
import com.dherrero.kotlincleanarchitecture.ui.base.BaseFragment
import com.dherrero.kotlincleanarchitecture.ui.login.view.LoginInterfaceConnectFragmentsWithActivity
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject


/**
 * Created by dherrero on 26/02/2018.
 */

class LoginFragment @Inject constructor() : BaseFragment(), View.OnClickListener {


    @Inject
    lateinit var interfaceForConnectWithLoginActivity: LoginInterfaceConnectFragmentsWithActivity

    fun newInstance(): LoginFragment {

        val args = Bundle()

        val fragment = LoginFragment()
        fragment.setArguments(args)
        return fragment
    }


    override fun getLayoutResource(): Int {

        return R.layout.fragment_login
    }

    override fun getMenuResource(): Int {

        return R.menu.menu_configuser

    }

    override fun setOptionsMenu(menu: Menu?) {


    }

    override fun setLogicWhenOnViewCreated() {
        sign_in_google_button.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v != null) {

            when (v.id) {

                sign_in_google_button.id -> signIn()

            }
        }
    }

    fun signIn() {

        interfaceForConnectWithLoginActivity.callSignIn()

    }

    override fun setView() {

    }
}