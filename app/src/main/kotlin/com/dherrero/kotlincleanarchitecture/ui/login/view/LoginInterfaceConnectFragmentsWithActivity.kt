package com.dherrero.kotlincleanarchitecture.ui.login.view

/**
 * Created by dherrero on 27/02/2018.
 */
interface LoginInterfaceConnectFragmentsWithActivity {
    fun callSignIn()
}