package com.dherrero.kotlincleanarchitecture.ui.configuser.presenter

import com.dherrero.kotlincleanarchitecture.model.User

/**
 * Created by dherrero on 28/02/2018.
 */
interface ConfigUserActivityPresenter{

    fun  createNewUser(firstname: String, surname: String, nickname: String, email: String, role: Int, photourl: String, superuser: Boolean, timestamp : Long, input: Boolean)

    fun getUsers(): Iterable<User>

    fun onDestroy()
}