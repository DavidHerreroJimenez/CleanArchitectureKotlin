package com.dherrero.kotlincleanarchitecture.ui.configuser.di

import com.dherrero.kotlincleanarchitecture.ui.configuser.ConfigUserActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

/**
 * Created by dherrero on 28/02/2018.
 */
@Subcomponent(modules = arrayOf(ConfigUserActivityModule::class, ConfigUserActivityModuleProvider::class))
interface ConfigUserActivitySubComponent : AndroidInjector<ConfigUserActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<ConfigUserActivity>()
}