package com.dherrero.kotlincleanarchitecture.ui.login.di

import com.dherrero.kotlincleanarchitecture.ui.login.LoginActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector


/**
 * Created by dherrero on 26/02/2018.
 */
@Subcomponent(modules = arrayOf(LoginActivityModule::class, LoginActivityModuleProvider::class))
interface LoginActivitySubComponent : AndroidInjector<LoginActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<LoginActivity>()


}