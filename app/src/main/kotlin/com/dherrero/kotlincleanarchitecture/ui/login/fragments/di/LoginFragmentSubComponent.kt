package com.dherrero.kotlincleanarchitecture.ui.login.fragments.di

import com.dherrero.kotlincleanarchitecture.ui.login.fragments.LoginFragment
import dagger.Subcomponent
import dagger.android.AndroidInjector


/**
 * Created by dherrero on 26/02/2018.
 */
@Subcomponent(modules = arrayOf(LoginFragmentModule::class))
interface LoginFragmentSubComponent : AndroidInjector<LoginFragment> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<LoginFragment>()
}