package com.dherrero.kotlincleanarchitecture.ui.configuser.fragments.di

import com.dherrero.kotlincleanarchitecture.ui.configuser.fragments.ConfigUserFragment
import dagger.Subcomponent
import dagger.android.AndroidInjector

/**
 * Created by dherrero on 28/02/2018.
 */
@Subcomponent(modules = arrayOf(ConfigUserFragmentModule::class))
interface ConfigUserFragmentComponent : AndroidInjector<ConfigUserFragment> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<ConfigUserFragment>()
}