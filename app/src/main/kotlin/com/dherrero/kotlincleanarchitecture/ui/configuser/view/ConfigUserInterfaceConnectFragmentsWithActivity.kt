package com.dherrero.kotlincleanarchitecture.ui.configuser.view

/**
 * Created by dherrero on 01/03/2018.
 */
interface ConfigUserInterfaceConnectFragmentsWithActivity {
    fun signOut()

    fun createNewUserIntoBD()

    fun getUsers()
}