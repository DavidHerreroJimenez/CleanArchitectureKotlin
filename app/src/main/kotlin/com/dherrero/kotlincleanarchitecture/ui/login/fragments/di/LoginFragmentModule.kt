package com.dherrero.kotlincleanarchitecture.ui.login.fragments.di

import android.support.v4.app.Fragment
import com.dherrero.kotlincleanarchitecture.ui.login.fragments.LoginFragment
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap

/**
 * Created by dherrero on 01/03/2018.
 */
@Module
abstract class LoginFragmentModule {

    @Binds
    @IntoMap
    @FragmentKey(LoginFragment::class)
    internal abstract fun bindLoginFragmentInjectorFactory(builder: LoginFragmentSubComponent.Builder): AndroidInjector.Factory<out Fragment>


}

