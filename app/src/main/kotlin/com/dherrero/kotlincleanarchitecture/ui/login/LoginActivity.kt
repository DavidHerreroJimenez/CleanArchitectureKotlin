package com.dherrero.kotlincleanarchitecture.ui.login

import android.content.Intent
import android.support.v4.app.Fragment
import android.widget.Toast
import com.dherrero.googlesignin.GoogleSignInApi
import com.dherrero.kotlincleanarchitecture.R
import com.dherrero.kotlincleanarchitecture.ui.base.BaseActivity
import com.dherrero.kotlincleanarchitecture.ui.configuser.ConfigUserActivity
import com.dherrero.kotlincleanarchitecture.ui.login.fragments.LoginFragment
import com.dherrero.kotlincleanarchitecture.ui.login.presenter.LoginActivityPresenter
import com.dherrero.kotlincleanarchitecture.ui.login.view.LoginActivityView
import com.dherrero.kotlincleanarchitecture.ui.login.view.LoginInterfaceConnectFragmentsWithActivity
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


/**
 * Created by dherrero on 26/02/2018.
 */
class LoginActivity : BaseActivity(), HasSupportFragmentInjector, LoginActivityView, LoginInterfaceConnectFragmentsWithActivity {


    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var loginActivityPresenter: LoginActivityPresenter

    @Inject
    lateinit var googleSignInApi: GoogleSignInApi


    val RC_SIGN_IN = 1


    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }


    override fun getLayoutResource(): Int {
        return R.layout.activity_login
    }

    override fun setLogicOnCreate() {

        googleSignInApi.getClientForSignInWithGoogle(this)

        supportFragmentManager.beginTransaction().add(R.id.login_fragment_container, LoginFragment().newInstance()).commitAllowingStateLoss()

    }


    override fun onStart() {
        super.onStart()

        var account = googleSignInApi.getLastSignedInAccount(this)

        updateUI(account)
    }

    override fun callSignIn() {

        var signInIntent: Intent = googleSignInApi.signIn()

        callStartActivityForResultForSignIn(signInIntent)
    }

    fun callStartActivityForResultForSignIn(signInIntent: Intent) {
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode.equals(RC_SIGN_IN)) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val account = googleSignInApi.getSignedInAccountFromIntent(data)
            updateUI(account)

        }

    }


    fun updateUI(account: GoogleSignInAccount?) {

        if (account != null) {

            val intentConfigUserActivity = Intent(this, ConfigUserActivity::class.java)

            startActivity(intentConfigUserActivity)

        } else {
            Toast.makeText(this, "Inicie sesión con su usuario habitual", Toast.LENGTH_LONG).show()
        }

    }


}