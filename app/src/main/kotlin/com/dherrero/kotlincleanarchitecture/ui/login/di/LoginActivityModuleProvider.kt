package com.dherrero.kotlincleanarchitecture.ui.login.di

import com.dherrero.googlesignin.GoogleSignInApi
import com.dherrero.googlesignin.GoogleSignInApiImpl
import dagger.Module
import dagger.Provides

/**
 * Created by dherrero on 02/03/2018.
 */
@Module
class LoginActivityModuleProvider {


    @Provides
    internal fun provideGoogleSignInApi(): GoogleSignInApi {
        return GoogleSignInApiImpl()
    }

}