package com.dherrero.kotlincleanarchitecture.ui.configuser.di

import com.dherrero.googlesignin.GoogleSignInApi
import com.dherrero.googlesignin.GoogleSignInApiImpl
import dagger.Module
import dagger.Provides

/**
 * Created by dherrero on 02/03/2018.
 */
@Module
class ConfigUserActivityModuleProvider {

    @Provides //Si necesitamos pasar parámetros como una vista a un presenter por ejemplo
    internal fun provideGoogleSignInApi(): GoogleSignInApi {
        return GoogleSignInApiImpl()
    }


}