package com.dherrero.kotlincleanarchitecture

import android.app.Activity
import android.app.Application
import com.dherrero.kotlincleanarchitecture.data.realm.db.RealmDB
import com.dherrero.kotlincleanarchitecture.data.realm.db.RealmDBImpl
import com.dherrero.kotlincleanarchitecture.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

/**
 * Created by dherrero on 23/02/2018.
 */
class App : Application(), HasActivityInjector {

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var realmDB: RealmDB


    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector
    }

    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent.builder().app(this).build().inject(this)

        realmDB.createDBFromApplication(this)

    }


}