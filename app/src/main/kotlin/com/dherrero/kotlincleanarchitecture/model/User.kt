package com.dherrero.kotlincleanarchitecture.model

/**
 * Created by dherrero on 08/03/2018.
 */
class User(

    var firstname: String = "",
    var surname: String = "",
    var nickname: String = "",
    var email: String = "",
    var role: Int = 0,
    var photourl: String = "",
    var superuser: Boolean = false,
    var time: Time? = null
)