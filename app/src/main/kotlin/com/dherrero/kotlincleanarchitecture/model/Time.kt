package com.dherrero.kotlincleanarchitecture.model

/**
 * Created by dherrero on 08/03/2018.
 */
class Time(

    var timestamp : Long = 0,

    var input : Boolean = false

    )