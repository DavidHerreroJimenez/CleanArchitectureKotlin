package com.dherrero.kotlincleanarchitecture.data.realm.db

import android.content.Context
import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmObject
import kotlin.reflect.KClass

/**
 * Created by dherrero on 19/03/2018.
 */
interface RealmDB{

    fun createDBFromApplication(context: Context)

    fun getInstanceDB(): Realm

    fun closeDB()

    fun createRealmObject(classObjectFamily: KClass<RealmObject>): RealmModel

}