package com.dherrero.kotlincleanarchitecture.data.realm.repository.converters

import com.dherrero.kotlincleanarchitecture.data.realm.repository.model.RealmUser
import com.dherrero.kotlincleanarchitecture.model.User
import io.realm.Realm

/**
 * Created by dherrero on 19/03/2018.
 */
interface UserToRealmUser{
    fun convertToRealmUser(realm: Realm, userToConvert: User): RealmUser
}