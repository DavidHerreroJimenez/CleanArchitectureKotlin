package com.dherrero.kotlincleanarchitecture.data.realm.repository.model

import io.realm.RealmObject

/**
 * Created by dherrero on 02/03/2018.
 */
open class RealmTimes : RealmObject() {

    open var timestamp : Long = 0

    open var its_input : Boolean = false

}