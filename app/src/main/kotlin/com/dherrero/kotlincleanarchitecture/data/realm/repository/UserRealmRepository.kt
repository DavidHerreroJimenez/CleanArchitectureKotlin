package com.dherrero.kotlincleanarchitecture.data.realm.repository

import com.dherrero.kotlincleanarchitecture.data.realm.db.RealmDB
import com.dherrero.kotlincleanarchitecture.data.realm.repository.base.BaseRealmRepository
import com.dherrero.kotlincleanarchitecture.data.realm.repository.model.RealmTimes
import com.dherrero.kotlincleanarchitecture.data.realm.repository.model.RealmUser
import com.dherrero.kotlincleanarchitecture.model.Time
import com.dherrero.kotlincleanarchitecture.model.User
import io.realm.Realm
import io.realm.RealmObject
import javax.inject.Inject

/**
 * Created by dherrero on 19/03/2018.
 */
class UserRealmRepository @Inject constructor(): BaseRealmRepository<RealmUser>(){


    fun convertToRealmObject(objectToConvert: User): RealmObject {

        var timesheet =  RealmTimes()

        timesheet.timestamp = objectToConvert.time!!.timestamp
        timesheet.its_input = objectToConvert.time!!.input


        var managedUser = RealmUser()

        managedUser.name = objectToConvert.firstname
        managedUser.surname = objectToConvert.surname
        managedUser.nickname = objectToConvert.nickname
        managedUser.email = objectToConvert.email
        managedUser.role = objectToConvert.role
        managedUser.photourl = objectToConvert.photourl
        managedUser.super_user = objectToConvert.superuser
        managedUser.times = timesheet

        return managedUser
    }

    private fun convertUserRealmToUser(realmUser: RealmUser): User {

        val time : Time = Time(

                realmUser.times!!.timestamp,

                realmUser.times!!.its_input
        )

        val user: User = User(

                realmUser.name,
                realmUser.surname,
                realmUser.nickname,
                realmUser.email,
                realmUser.role,
                realmUser.photourl,
                realmUser.super_user,
                time


        )



        return user

    }


    fun createUser(item: User): User {

        val itemConverted = convertToRealmObject(item)

        val itemCreatedIntoRealmDB = create(itemConverted as RealmUser)

        val userCreated = convertUserRealmToUser(itemCreatedIntoRealmDB)

        return userCreated

    }

    fun findAllUsers(): Iterable<User> {

        val iterableRealmUsers = findAllRealmUsers(RealmUser::class.java)


        var iterableUsers: Iterable<User> = arrayListOf()

        for (item in iterableRealmUsers){

            val user = convertUserRealmToUser(item)

            iterableUsers.plusElement(user)

        }

        return iterableUsers
    }
}