package com.dherrero.kotlincleanarchitecture.data.realm.repository.converters

import com.dherrero.kotlincleanarchitecture.data.realm.repository.model.RealmUser
import com.dherrero.kotlincleanarchitecture.model.User

/**
 * Created by dherrero on 08/03/2018.
 */
interface UserRealmToUser{

    fun convertUserRealmToUser(realmUser: RealmUser): User

}