package com.dherrero.kotlincleanarchitecture.data.base


import io.realm.RealmModel

import io.realm.RealmResults

/**
 * Created by dherrero on 08/03/2018.
 */
interface BaseRepository<T> {

    fun create(item: T) : T

    fun remove(item: T)

    fun update(item: T) : T

    fun find(item: T): T

    fun <T:RealmModel> findAllRealmUsers(clazz: Class<T>): List<T>

}