package com.dherrero.kotlincleanarchitecture.data.realm.repository.converters

import com.dherrero.kotlincleanarchitecture.data.realm.repository.model.RealmUser
import com.dherrero.kotlincleanarchitecture.model.Time
import com.dherrero.kotlincleanarchitecture.model.User

/**
 * Created by dherrero on 08/03/2018.
 */
class UserRealmToUserImpl: UserRealmToUser {


    override fun convertUserRealmToUser(realmUser: RealmUser): User {

        val time : Time = Time(

                realmUser.times!!.timestamp,

                realmUser.times!!.its_input
        )

        val user: User = User(

                realmUser.name,
                realmUser.surname,
                realmUser.nickname,
                realmUser.email,
                realmUser.role,
                realmUser.photourl,
                realmUser.super_user,
                time


        )



        return user

    }
}