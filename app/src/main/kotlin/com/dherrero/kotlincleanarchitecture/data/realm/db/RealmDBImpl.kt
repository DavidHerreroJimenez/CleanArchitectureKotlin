package com.dherrero.kotlincleanarchitecture.data.realm.db

import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmModel
import io.realm.RealmObject
import javax.inject.Inject
import kotlin.reflect.KClass

/**
 * Created by dherrero on 19/03/2018.
 */
class RealmDBImpl @Inject constructor(): RealmDB{


    private lateinit var realm: Realm

    private fun initRealm(context: Context){

        Realm.init(context)

    }

    private fun buildBD() : RealmConfiguration {
        val config = RealmConfiguration.Builder().name("kotlinCleanArchitectureBD.realm").build()

        return config
    }

    private fun setDefaultConfigurationDB(configDb : RealmConfiguration){

        Realm.setDefaultConfiguration(configDb)
    }


    private fun configureDB(){

        val config = buildBD()
        setDefaultConfigurationDB(config)
    }

    private fun createDB(context: Context){
        initRealm(context)

        configureDB()
    }

    override
    fun createDBFromApplication(context: Context){

        createDB(context)
    }
    override
    fun getInstanceDB(): Realm{

        realm = Realm.getDefaultInstance()

       return realm
    }
    override
    fun closeDB() {

        if (realm != null) {
            realm.close()
        }
    }


    override
    fun createRealmObject(classObjectFamily: KClass<RealmObject>): RealmModel {

        val realmObject = realm.createObject(classObjectFamily.java)

        return realmObject
    }
}