package com.dherrero.kotlincleanarchitecture.data.base


import com.dherrero.kotlincleanarchitecture.data.realm.repository.model.RealmUser
import io.realm.*
import io.realm.RealmResults


/**
 * Created by dherrero on 16/03/2018.
 */
abstract class BaseRepositoryImpl<T> : BaseRepository<T> {

    override fun create(item: T): T {

        when (item){
           is RealmObject -> insertIntoRealmDB(item)
            else -> throw IllegalArgumentException("Se intenta crear un tipo de objeto erroneo")
        }

        return item
    }


    private fun insertIntoRealmDB(item: RealmModel){

        val realm: Realm = Realm.getDefaultInstance()


        realm.beginTransaction()
        try {

            realm.copyToRealm(item)
            realm.commitTransaction()
        }catch (e: Exception){
            realm.cancelTransaction()
        }finally {
            realm.close()
        }
    }

    override fun remove(item: T) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun update(item: T): T {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun find(item: T): T {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override   fun <T:RealmModel> findAllRealmUsers(clazz: Class<T>): List<T>{

        val realm = Realm.getDefaultInstance()

        val realmResults = realm
                .where(clazz)
                .findAll()

        val allResults: List<T> = realm.copyFromRealm(realmResults)

        return allResults
    }

//    override fun findAllRealmUsers(clazz: T): List<T> {
//
//
////        var realm: Realm = Realm.getDefaultInstance()
////
////         val query : RealmQuery<T> = realm.where(clazz)
////
////        val realmResults : RealmResults<T> = query.findAll()
////
////        val allResults: List<T> = realm.copyFromRealm(realmResults);
//
//        val allResults = ArrayList<T>()
//        return allResults
//    }








}