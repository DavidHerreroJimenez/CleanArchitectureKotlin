package com.dherrero.kotlincleanarchitecture.data.realm.repository.base

import android.content.Context
import com.dherrero.kotlincleanarchitecture.data.base.BaseRepositoryImpl
import com.dherrero.kotlincleanarchitecture.data.realm.db.RealmDB
import com.dherrero.kotlincleanarchitecture.data.realm.repository.model.RealmTimes
import com.dherrero.kotlincleanarchitecture.data.realm.repository.model.RealmUser
import com.dherrero.kotlincleanarchitecture.model.User
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmModel
import io.realm.RealmObject
import javax.inject.Inject
import kotlin.reflect.KClass


/**
 * Created by dherrero on 08/03/2018.
 */
abstract class BaseRealmRepository<T> : BaseRepositoryImpl<T>(){

    @Inject
    lateinit var realmDB: RealmDB

    fun getInstance(){

        realmDB.getInstanceDB()

    }

    fun createObject(item: KClass<RealmObject>): RealmModel{

        return realmDB.createRealmObject(item)
    }


    fun closeDB(){
        realmDB.closeDB()
    }






//    override fun create(item: User): User {
//
//        super.create(item)
//
////        val realm: Realm = Realm.getDefaultInstance()
////
////        realm.beginTransaction()
////
////
////        var timesheet = realm.createObject(RealmTimes::class.java)
////
////        timesheet.timestamp = item.time!!.timestamp
////        timesheet.its_input = item.time!!.input
////
////
////        var managedUser = realm.createObject(RealmUser::class.java)
////
////        managedUser.name = item.firstname
////        managedUser.surname = item.surname
////        managedUser.nickname = item.nickname
////        managedUser.email = item.email
////        managedUser.role = item.role
////        managedUser.photourl = item.photourl
////        managedUser.super_user = item.superuser
////        managedUser.times = timesheet
////
////
////        realm.copyToRealm(managedUser)
////        realm.commitTransaction()
////
////
////        realm.close()
//        return item
//    }


    //    override fun add(item: User) {
//
//        val realm: Realm = Realm.getDefaultInstance()
//
//        realm.beginTransaction()
//
//
//        var timesheet = realm.createObject(RealmTimes::class.java)
//
//        timesheet.timestamp = item.time!!.timestamp
//        timesheet.its_input = item.time!!.input
//
//
//        var managedUser = realm.createObject(RealmUser::class.java)
//
//        managedUser.name = item.firstname
//        managedUser.surname = item.surname
//        managedUser.nickname = item.nickname
//        managedUser.email = item.email
//        managedUser.role = item.role
//        managedUser.photourl = item.photourl
//        managedUser.super_user = item.superuser
//        managedUser.times = timesheet
//
//
//        realm.copyToRealm(managedUser)
//        realm.commitTransaction()
//
//
//        realm.close()
//
//    }
//
//    override fun update(item: User) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun remove(item: User) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//
//    override fun get(): List<User> {
//
//        val realm: Realm = Realm.getDefaultInstance()
//
//
//            val userRealmToUser: UserRealmToUser = UserRealmToUserImpl()
//
//            // Build the query looking at all users:
//            val query = realm.where(RealmUser::class.java)
//
//            // Execute the query:
//            val result: RealmResults<RealmUser> = query.findAll()
//
//
//            var userList: List<User> = emptyList()
//
//
//            for(userRealm: RealmUser in result){
//
//                userList.plusElement(userRealmToUser.convertUserRealmToUser(userRealm))
//
//            }
//
//
//        realm.close()
//
//            return userList
//
//    }
//
//


}