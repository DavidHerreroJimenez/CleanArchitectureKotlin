package com.dherrero.kotlincleanarchitecture.data.realm.repository.model

import io.realm.RealmObject

/**
 * Created by dherrero on 02/03/2018.
 */
open class RealmUser : RealmObject(){



        open var name: String = ""
        open var surname: String = ""
        open var nickname: String = ""
        open var email: String = ""
        open var role: Int = 0
        open var photourl: String = ""
        open var super_user: Boolean = false
        open var times: RealmTimes? = null

}