package com.dherrero.kotlincleanarchitecture.data.realm.repository.converters

import com.dherrero.kotlincleanarchitecture.data.realm.repository.model.RealmTimes
import com.dherrero.kotlincleanarchitecture.data.realm.repository.model.RealmUser
import com.dherrero.kotlincleanarchitecture.model.User
import io.realm.Realm
import javax.inject.Inject

/**
 * Created by dherrero on 19/03/2018.
 */
class UserToRealmUserImpl @Inject constructor() : UserToRealmUser{

    override fun convertToRealmUser(realm: Realm, userToConvert: User): RealmUser {


        var timesheet = realm.createObject(RealmTimes::class.java)

        timesheet.timestamp = userToConvert.time!!.timestamp
        timesheet.its_input = userToConvert.time!!.input


        var managedUser = realm.createObject(RealmUser::class.java)

        managedUser.name = userToConvert.firstname
        managedUser.surname = userToConvert.surname
        managedUser.nickname = userToConvert.nickname
        managedUser.email = userToConvert.email
        managedUser.role = userToConvert.role
        managedUser.photourl = userToConvert.photourl
        managedUser.super_user = userToConvert.superuser
        managedUser.times = timesheet

        return managedUser
    }
}