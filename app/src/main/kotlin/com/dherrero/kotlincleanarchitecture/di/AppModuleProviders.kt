package com.dherrero.kotlincleanarchitecture.di

import android.app.Application
import android.content.Context
import com.dherrero.kotlincleanarchitecture.data.base.BaseRepository
import com.dherrero.kotlincleanarchitecture.data.realm.db.RealmDB
import com.dherrero.kotlincleanarchitecture.data.realm.db.RealmDBImpl
import com.dherrero.kotlincleanarchitecture.data.realm.repository.UserRealmRepository
import com.dherrero.kotlincleanarchitecture.data.realm.repository.model.RealmUser
import com.dherrero.kotlincleanarchitecture.model.User
import dagger.Module
import dagger.Provides

/**
 * Created by dherrero on 05/03/2018.
 */
@Module
class AppModuleProviders{


    @Provides
    internal fun provideRealmDB(realmDBImpl: RealmDBImpl): RealmDB{

        return realmDBImpl
    }

    @Provides
    internal fun provideUserRealmRepository(userRealmRespository: UserRealmRepository): BaseRepository<RealmUser> {

        return userRealmRespository
    }

    @Provides
//    @Singleton
    internal fun provideContext(app: Application): Context {
        return app
    }






}