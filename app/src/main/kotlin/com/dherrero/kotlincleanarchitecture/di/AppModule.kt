package com.dherrero.kotlincleanarchitecture.di

import android.app.Activity
import com.dherrero.kotlincleanarchitecture.model.Time
import com.dherrero.kotlincleanarchitecture.model.User
import com.dherrero.kotlincleanarchitecture.ui.configuser.ConfigUserActivity
import com.dherrero.kotlincleanarchitecture.ui.configuser.di.ConfigUserActivitySubComponent
import com.dherrero.kotlincleanarchitecture.ui.login.LoginActivity
import com.dherrero.kotlincleanarchitecture.ui.login.di.LoginActivitySubComponent
import dagger.Binds
import dagger.Module
import dagger.android.ActivityKey
import dagger.android.AndroidInjector
import dagger.multibindings.IntoMap

/**
 * Created by dherrero on 23/02/2018.
 */
@Module(subcomponents = arrayOf(LoginActivitySubComponent::class, ConfigUserActivitySubComponent::class))
abstract class AppModule {

    @Binds
    @IntoMap
    @ActivityKey(LoginActivity::class)
    internal abstract fun bindLoginActivity(builder: LoginActivitySubComponent.Builder): AndroidInjector.Factory<out Activity>


    @Binds
    @IntoMap
    @ActivityKey(ConfigUserActivity::class)
    internal abstract fun bindConfigUserActivity(builder: ConfigUserActivitySubComponent.Builder): AndroidInjector.Factory<out Activity>


}