package com.dherrero.kotlincleanarchitecture.di

import android.app.Application
import com.dherrero.kotlincleanarchitecture.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


/**
 * Created by dherrero on 23/02/2018.
 */
@Component(modules = arrayOf(AndroidInjectionModule::class, AppModule::class, AppModuleProviders::class))
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun app(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}