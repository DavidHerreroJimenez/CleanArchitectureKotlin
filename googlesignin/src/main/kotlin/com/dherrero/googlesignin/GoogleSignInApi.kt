package com.dherrero.googlesignin

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.OnCompleteListener

/**
 * Created by dherrero on 26/02/2018.
 */
interface GoogleSignInApi{

    fun getClientForSignInWithGoogle(context : Context)
    fun getLastSignedInAccount(context : Context) : GoogleSignInAccount?
    fun signIn() : Intent

    fun signOut(onCompleteListener: OnCompleteListener<Void>)

    fun getSignedInAccountFromIntent(data: Intent?) : GoogleSignInAccount?
}