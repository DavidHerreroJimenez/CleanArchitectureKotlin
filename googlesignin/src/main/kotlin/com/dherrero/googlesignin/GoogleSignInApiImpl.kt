package com.dherrero.googlesignin

import android.content.Context
import android.content.Intent
import android.util.Log
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task

/**
 * Created by dherrero on 26/02/2018.
 */
class GoogleSignInApiImpl : GoogleSignInApi {


    lateinit var mGoogleSignInClient : GoogleSignInClient

    override fun getClientForSignInWithGoogle(context : Context) {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(context, gso)


    }

    override
    fun getLastSignedInAccount(context : Context): GoogleSignInAccount? {

        var account = GoogleSignIn.getLastSignedInAccount(context)


        return account
    }

    override fun signIn(): Intent {
        val signInIntent = mGoogleSignInClient.getSignInIntent()


        return signInIntent

    }

    override fun signOut(onCompleteListener: OnCompleteListener<Void>) {

        mGoogleSignInClient.signOut()
                .addOnCompleteListener(onCompleteListener)
    }

    override fun getSignedInAccountFromIntent(data: Intent?): GoogleSignInAccount? {
        val task = GoogleSignIn.getSignedInAccountFromIntent(data)

        val account = handleSignInResult(task)


        return account
    }


    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) : GoogleSignInAccount? {
        var account : GoogleSignInAccount?

        try {
            account = completedTask.getResult(ApiException::class.java)

        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("handleSignInResult", "signInResult:failed code=" + e.statusCode)

            account = null
        }

        return account

    }
}